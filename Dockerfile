# ---------------------------------------------------------------- build stage
# get build image
FROM golang:1.15 as builder

# set build working directory
WORKDIR /app

# copy the source code
COPY . .

# build the project
RUN go build gitlab.com/rezakamalifard/find-property/cmd/server

# -------------------------------------------------------------- release stage
# get runtime image
FROM debian:10-slim

# copy built binary to runtime environment
COPY --from=builder /app/server /bin

# Expose port 8080 to the outside world
EXPOSE 8080

# run it!
CMD ["server"]