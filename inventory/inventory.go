package inventory

import (
	"bufio"
	"compress/gzip"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	"go.uber.org/zap"
)

const (
	alpha = "alpha"
	beta  = "beta"
	gamma = "gamma"

	bufferMaxCapacity = 1024 * 1024
)

type store interface {
	StoreProperty(property map[string]interface{}) error
	GetMapping(id, kind string) (master string, err error)
}

type property struct {
	MasterID    string  `structs:"master_id"`
	ID          string  `structs:"id"`
	Name        string  `structs:"name"`
	Address     string  `structs:"address"`
	StarRating  int     `structs:"star_rating"`
	Latitude    float64 `structs:"latitude"`
	Longitude   float64 `structs:"longitude"`
	CountryCode string  `structs:"country_code"`
	City        string  `structs:"city"`
	Kind        string  `structs:"kind"`
}

type file struct {
	name     string
	fullName string
	path     string
	kind     string
}

// Inventory type is used to store store data
type Inventory struct {
	Store store
}

// NewInventory creates a new Inventory instance assign it's variables and return it
func NewInventory(s store) *Inventory {
	return &Inventory{Store: s}
}

// Process parse a directory of inventory files and add them to store
func (i *Inventory) Process(path string) error {
	if path == "" {
		return errors.New("inventories path is empty")
	}

	var files []file
	err := filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		fullName := info.Name()
		ext := filepath.Ext(fullName)
		if !info.IsDir() || ext == ".gz" {
			name := fullName[:strings.IndexByte(fullName, '.')]
			ext := fullName[strings.IndexByte(fullName, '.')+1:]
			kind := ext[:strings.IndexByte(ext, '.')]
			files = append(files, file{path: path, fullName: fullName, name: name, kind: kind})
		}
		return nil
	})
	if err != nil {
		return err
	}

	for _, file := range files {
		f, err := os.Open(file.path)
		if err != nil {
			return err
		}
		zipread, err := gzip.NewReader(f)
		if err != nil {
			return err
		}
		buf := bufio.NewReader(zipread)

		switch file.name {
		case alpha:
			started := time.Now()
			successful, failed, err := i.processAlpha(buf)
			if err != nil {
				zap.L().Error("processing alpha failed", zap.Errors("errors", []error{err}))
			}
			duration := time.Since(started)
			zap.L().Info(fmt.Sprintf("processed %s", file.name),
				zap.Uint64("successful",
					successful),
				zap.Uint64("failed", failed),
				zap.String("duration", duration.String()),
			)
		case beta:
			started := time.Now()
			successful, failed, err := i.processBeta(buf)
			if err != nil {
				zap.L().Error("processing beta failed", zap.Errors("errors", []error{err}))
			}
			duration := time.Since(started)
			zap.L().Info(fmt.Sprintf("processed %s", file.name),
				zap.Uint64("successful",
					successful),
				zap.Uint64("failed", failed),
				zap.String("duration", duration.String()),
			)
		case gamma:
			started := time.Now()
			successful, failed, err := i.processGamma(buf)
			if err != nil {
				zap.L().Error("processing gamma failed", zap.Errors("errors", []error{err}))
			}
			duration := time.Since(started)
			zap.L().Info(fmt.Sprintf("processed %s", file.name),
				zap.Uint64("successful",
					successful),
				zap.Uint64("failed", failed),
				zap.String("duration", duration.String()),
			)

		default:
			zap.L().Warn("partner not supported", zap.String("kind", file.name))
		}
	}
	return nil
}
