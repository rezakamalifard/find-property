package inventory

import (
	"bufio"
	"encoding/json"
	"io"
	"time"

	"github.com/fatih/structs"
	"go.uber.org/zap"
)

func (i *Inventory) processAlpha(r *bufio.Reader) (uint64, uint64, error) {
	var successful, failed uint64
	for {
		line, err := r.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			failed++
			return successful, failed, err
		}
		// parse json record in the JSONL file
		var p alphaProperty
		err = json.Unmarshal([]byte(line), &p)
		if err != nil {
			zap.L().Error("json unmarshall failed", zap.Errors("errors", []error{err}))
			failed++
			continue
		}
		// Get Master Id from mapper
		masterID, err := i.Store.GetMapping(p.ID, alpha)
		if err != nil {
			zap.L().Error("getting master id from mapper failed", zap.Errors("errors", []error{err}))
			failed++
			continue
		}

		// create property instance
		pr := property{
			MasterID:    masterID,
			ID:          p.ID,
			Name:        p.Name.En,
			Address:     p.Address.StreetAddress.En,
			Latitude:    p.Address.Latitude,
			Longitude:   p.Address.Longitude,
			CountryCode: p.Address.CountryCode,
			City:        p.Address.Locality.En,
			Kind:        alpha,
		}
		// store property
		mapped := structs.Map(pr)
		if err = i.Store.StoreProperty(mapped); err != nil {
			failed++
			continue
		}
		// increment successful counter
		successful++
	}
	return successful, failed, nil
}

type alphaProperty struct {
	Address struct {
		CountryCode string  `json:"country_code"`
		Latitude    float64 `json:"latitude"`
		Locality    struct {
			En string `json:"en"`
		} `json:"locality"`
		Longitude  float64 `json:"longitude"`
		PostalCode string  `json:"postal_code"`
		Region     struct {
		} `json:"region"`
		StreetAddress struct {
			En string `json:"en"`
		} `json:"street_address"`
	} `json:"address"`
	Amenities []struct {
		ID   string `json:"id"`
		Name struct {
			En string `json:"en"`
		} `json:"name"`
	} `json:"amenities"`
	Brand struct {
		ID   interface{} `json:"id"`
		Name struct {
		} `json:"name"`
	} `json:"brand"`
	Chain struct {
		ID   interface{} `json:"id"`
		Name struct {
		} `json:"name"`
	} `json:"chain"`
	CheckInStartTime string    `json:"check_in_start_time"`
	CheckOutEndTime  string    `json:"check_out_end_time"`
	ContentUpdatedAt time.Time `json:"content_updated_at"`
	CurrencyCode     string    `json:"currency_code"`
	Description      struct {
		En string `json:"en"`
	} `json:"description"`
	EssentialWorkersOnly bool   `json:"essential_workers_only"`
	ID                   string `json:"id"`
	Images               []struct {
		Height int    `json:"height"`
		ID     string `json:"id"`
		Urls   struct {
			Large string `json:"large"`
		} `json:"urls"`
		Version int `json:"version"`
		Width   int `json:"width"`
	} `json:"images"`
	LocationDescription struct {
		En string `json:"en"`
	} `json:"location_description"`
	MergedIds []interface{} `json:"merged_ids"`
	Name      struct {
		En string `json:"en"`
	} `json:"name"`
	PhoneNumber       interface{} `json:"phone_number"`
	PolicyDescription struct {
		En string `json:"en"`
	} `json:"policy_description"`
	PropertyType struct {
		ID   interface{} `json:"id"`
		Name struct {
		} `json:"name"`
	} `json:"property_type"`
	RoomTypes []struct {
		Amenities   []interface{} `json:"amenities"`
		Description struct {
			En string `json:"en"`
		} `json:"description"`
		ID     string        `json:"id"`
		Images []interface{} `json:"images"`
		Name   struct {
			En string `json:"en"`
		} `json:"name"`
		SquareFootage interface{} `json:"square_footage"`
	} `json:"room_types"`
	StarRating interface{} `json:"star_rating"`
	TimeZone   string      `json:"time_zone"`
}
