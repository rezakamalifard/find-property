package inventory

import (
	"compress/gzip"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rezakamalifard/find-property/storage"
	"os"
	"path"
	"testing"
)

const alphaFileName = "alpha.json.gz"

func mockAlphaFile(t *testing.T, records []alphaProperty) (path string) {
	t.Helper()
	dir := os.TempDir()
	alphaDir := dir + "alpha-tests/"
	createDirIfNotExist(t, alphaDir)
	fileName := alphaDir + alphaFileName
	f, err := os.Create(fileName)
	defer f.Close()

	assert.NoError(t, err)

	w := gzip.NewWriter(f)
	defer w.Close()

	for _, record := range records {
		b, err := json.Marshal(record)
		_, writeErr := w.Write(b)
		_, writeNewLineErr := fmt.Fprintln(w)

		assert.NoError(t, err)
		assert.NoError(t, writeErr)
		assert.NoError(t, writeNewLineErr)
	}

	return f.Name()
}

func TestAlpha(t *testing.T) {
	// running these tests need to connect to database
	if testing.Short() {
		t.Skip("skipping test in short mode")
	}
	records := []alphaProperty{
		{
			ID: "5",
			Name: struct {
				En string `json:"en"`
			}{En: "Hotel 1"},
			Address: struct {
				CountryCode string  `json:"country_code"`
				Latitude    float64 `json:"latitude"`
				Locality    struct {
					En string `json:"en"`
				} `json:"locality"`
				Longitude  float64 `json:"longitude"`
				PostalCode string  `json:"postal_code"`
				Region     struct {
				} `json:"region"`
				StreetAddress struct {
					En string `json:"en"`
				} `json:"street_address"`
			}{StreetAddress: struct {
				En string `json:"en"`
			}{En: "Address 2"}, Latitude: 1.1, Longitude: 1.2, CountryCode: "1 CC", Locality: struct {
				En string `json:"en"`
			}{En: "1 City"}},
		},
		{
			ID: "25",
			Name: struct {
				En string `json:"en"`
			}{En: "Hotel 2"},
			Address: struct {
				CountryCode string  `json:"country_code"`
				Latitude    float64 `json:"latitude"`
				Locality    struct {
					En string `json:"en"`
				} `json:"locality"`
				Longitude  float64 `json:"longitude"`
				PostalCode string  `json:"postal_code"`
				Region     struct {
				} `json:"region"`
				StreetAddress struct {
					En string `json:"en"`
				} `json:"street_address"`
			}{StreetAddress: struct {
				En string `json:"en"`
			}{En: "Address 2"}, Latitude: 2.1, Longitude: 2.2, CountryCode: "2 CC", Locality: struct {
				En string `json:"en"`
			}{En: "2 City"}},
		},
	}
	p := mockAlphaFile(t, records)
	dir, _ := path.Split(p)

	// create store
	s, err := storage.NewStorage()
	assert.NoError(t, err)

	err = s.StoreMapping("10", "5", "20", "1")
	assert.NoError(t, err)
	err = s.StoreMapping("20", "25", "30", "2")
	assert.NoError(t, err)

	i := NewInventory(s)
	err = i.Process(dir)
	assert.NoError(t, err)

	resp := make(map[string]interface{})
	resp, exists, err := s.GetProperty("10", alpha)
	assert.True(t, exists)
	assert.Equal(t, resp["name"], "Hotel 1")

	resp, exists, err = s.GetProperty("20", alpha)
	assert.True(t, exists)
	assert.Equal(t, resp["name"], "Hotel 2")
	assert.Equal(t, resp["city"], "2 City")
}
