package inventory

import (
	"bufio"
	"encoding/json"
	"io"
	"strconv"

	"github.com/fatih/structs"

	"go.uber.org/zap"
)

type gammaProperty struct {
	Addressen      string `json:"addressen"`
	Citynameen     string `json:"citynameen"`
	Countrynameen  string `json:"countrynameen"`
	Hotelid        string `json:"hotelid"`
	Hotelnameen    string `json:"hotelnameen"`
	Ida            string `json:"ida"`
	Idb            string `json:"idb"`
	Ide            string `json:"ide"`
	Latitude       string `json:"latitude"`
	Longitude      string `json:"longitude"`
	Provincenameen string `json:"provincenameen"`
	Telephone      string `json:"telephone"`
	Updatedate     string `json:"updatedate"`
	Zipcode        string `json:"zipcode"`
}

func (i *Inventory) processGamma(r *bufio.Reader) (uint64, uint64, error) {
	var successful, failed uint64
	for {
		line, _, err := r.ReadLine()
		if err == io.EOF {
			break
		}
		if err != nil {
			failed++
			return successful, failed, err
		}
		// parse json record in the JSONL file
		var p gammaProperty
		err = json.Unmarshal(line, &p)
		if err != nil {
			zap.L().Warn("json unmarshall failed", zap.Errors("errors", []error{err}))
			failed++
			continue
		}
		latitude, err := strconv.ParseFloat(p.Latitude, 64)
		if err != nil {
			failed++
			continue
		}
		longitude, err := strconv.ParseFloat(p.Longitude, 64)
		if err != nil {
			failed++
			continue
		}

		// Get Master Id from mapper
		masterID, err := i.Store.GetMapping(p.Hotelid, gamma)
		if err != nil {
			zap.L().Error("getting master id from mapper failed", zap.Errors("errors", []error{err}))
			failed++
			continue
		}

		pr := property{
			ID:        p.Hotelid,
			MasterID:  masterID,
			Name:      p.Hotelnameen,
			Address:   p.Addressen,
			Latitude:  latitude,
			Longitude: longitude,
			//TODO convert it iso
			CountryCode: p.Countrynameen,
			City:        p.Citynameen,
			Kind:        gamma,
		}
		// store property
		mapped := structs.Map(pr)
		if err = i.Store.StoreProperty(mapped); err != nil {
			failed++
			continue
		}
		// increment successful counter
		successful++
	}
	return successful, failed, nil
}
