package inventory

import (
	"compress/gzip"
	"encoding/csv"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rezakamalifard/find-property/storage"
	"os"
	"path"
	"testing"
)

const betaFileName = "beta.csv.gz"

func mockMappingFile(t *testing.T, records [][]string) (path string) {
	t.Helper()
	dir := os.TempDir()
	betaDir := dir + "beta-tests/"
	createDirIfNotExist(t, betaDir)
	fileName := betaDir + betaFileName
	f, err := os.Create(fileName)
	defer f.Close()

	assert.NoError(t, err)

	gzipW := gzip.NewWriter(f)
	defer gzipW.Close()

	csvW := csv.NewWriter(gzipW)

	for _, record := range records {
		err = csvW.Write(record)
		assert.NoError(t, err)
	}

	csvW.Flush()
	assert.NoError(t, csvW.Error())

	return f.Name()
}

func TestBeta(t *testing.T) {
	// running these tests need to connect to database
	if testing.Short() {
		t.Skip("skipping test in short mode")
	}
	records := [][]string{
		{"accommodationtype", "addressline1", "brandid", "brandname", "chainid", "chainname", "checkin", "checkout", "city", "cityid", "continentid", "continentname", "country", "countryid", "countryisocode", "hotelid", "hotelname", "hoteltranslatedname", "latitude", "longitude", "numberfloors", "numberofreviews", "numberrooms", "overview", "phone", "photo1", "photo2", "photo3", "photo4", "photo5", "ratescurrency", "ratesfrom", "ratesfromexclusive", "ratingaverage", "starrating", "state", "url", "zipcode", "hotelformerlyname", "yearopened", "yearrenovated", "addressline2"},
		{"", "address 1", "", "", "", "", "", "", "1 City", "", "", "", "", "", "1 CC", "1", "Hotel 1", "", "1.1", "1.2", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "5", "", "", "", "", "", "", ""},
		{"", "address 2", "", "", "", "", "", "", "2 City", "", "", "", "", "", "2 CC", "2", "Hotel 2", "", "2.1", "2.2", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "5", "", "", "", "", "", "", ""},
	}
	p := mockMappingFile(t, records)
	dir, _ := path.Split(p)

	// create store
	s, err := storage.NewStorage()
	assert.NoError(t, err)

	err = s.StoreMapping("10", "", "1", "")
	assert.NoError(t, err)
	err = s.StoreMapping("20", "", "2", "")
	assert.NoError(t, err)

	i := NewInventory(s)
	err = i.Process(dir)
	assert.NoError(t, err)

	resp := make(map[string]interface{})
	resp, exists, err := s.GetProperty("10", beta)
	assert.True(t, exists)
	assert.Equal(t, resp["name"], "Hotel 1")
	assert.Equal(t, resp["city"], "1 City")

	resp, exists, err = s.GetProperty("20", beta)
	assert.True(t, exists)
	assert.Equal(t, resp["name"], "Hotel 2")
	assert.Equal(t, resp["city"], "2 City")

}
