package inventory

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func createDirIfNotExist(t *testing.T, dir string) {
	t.Helper()
	_, err := os.Stat(dir)

	if os.IsNotExist(err) {
		errDir := os.MkdirAll(dir, os.ModePerm)
		assert.NoError(t, errDir)
	}
}