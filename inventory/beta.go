package inventory

import (
	"encoding/csv"
	"io"
	"strconv"

	"github.com/fatih/structs"

	"go.uber.org/zap"
)

func (i *Inventory) processBeta(r io.Reader) (uint64, uint64, error) {
	var successful, failed uint64
	reader := csv.NewReader(r)
	var header []string
	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return successful, failed, err
		}
		// skip headers
		if header == nil {
			header = record
			continue
		}

		row := make(map[string]string)
		for i := range header {
			row[header[i]] = record[i]
		}

		startRating, err := strconv.ParseFloat(row["starrating"], 64)
		if err != nil {
			failed++
			continue
		}
		latitude, err := strconv.ParseFloat(row["latitude"], 64)
		if err != nil {
			failed++
			continue
		}
		longitude, err := strconv.ParseFloat(row["longitude"], 64)
		if err != nil {
			failed++
			continue
		}
		id := row["hotelid"]
		// Get Master Id from mapper
		masterID, err := i.Store.GetMapping(id, beta)
		if err != nil {
			zap.L().Error("getting master id from mapper failed", zap.Errors("errors", []error{err}))
			failed++
			continue
		}

		pr := property{
			ID:          id,
			MasterID:    masterID,
			Name:        row["hotelname"],
			Address:     row["addressline1"],
			StarRating:  int(startRating),
			Latitude:    latitude,
			Longitude:   longitude,
			CountryCode: row["countryisocode"],
			City:        row["city"],
			Kind:        beta,
		}
		// store property
		mapped := structs.Map(pr)
		if err = i.Store.StoreProperty(mapped); err != nil {
			failed++
			continue
		}
		// increment successful counter
		successful++
	}
	return successful, failed, nil
}
