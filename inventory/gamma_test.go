package inventory

import (
	"compress/gzip"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rezakamalifard/find-property/storage"
	"os"
	"path"
	"testing"
)

const gammaFileName = "gamma.json.gz"

func mockGammaFile(t *testing.T, records []gammaProperty) (path string) {
	t.Helper()
	dir := os.TempDir()
	gammaDir := dir + "gamma-tests/"
	createDirIfNotExist(t, gammaDir)
	fileName := gammaDir + gammaFileName
	f, err := os.Create(fileName)
	defer f.Close()

	assert.NoError(t, err)

	w := gzip.NewWriter(f)
	defer w.Close()

	for _, record := range records {
		b, err := json.Marshal(record)
		_, writeErr := w.Write(b)
		_, writeNewLineErr := fmt.Fprintln(w)

		assert.NoError(t, err)
		assert.NoError(t, writeErr)
		assert.NoError(t, writeNewLineErr)
	}

	return f.Name()
}

func TestGamma(t *testing.T) {
	// running these tests need to connect to database
	if testing.Short() {
		t.Skip("skipping test in short mode")
	}
	records := []gammaProperty{
		{
			Hotelid:       "1",
			Hotelnameen:   "Hotel 1",
			Addressen:     "Address 1",
			Latitude:      "1.1",
			Longitude:     "1.2",
			Countrynameen: "1 CC",
			Citynameen:    "1 City",
		},
		{
			Hotelid:       "2",
			Hotelnameen:   "Hotel 2",
			Addressen:     "Address 2",
			Latitude:      "2.1",
			Longitude:     "2.2",
			Countrynameen: "2 CC",
			Citynameen:    "2 City",
		},
	}
	p := mockGammaFile(t, records)
	dir, _ := path.Split(p)

	// create store
	s, err := storage.NewStorage()
	assert.NoError(t, err)

	err = s.StoreMapping("10", "", "", "1")
	assert.NoError(t, err)
	err = s.StoreMapping("20", "", "", "2")
	assert.NoError(t, err)

	i := NewInventory(s)
	err = i.Process(dir)
	assert.NoError(t, err)

	resp := make(map[string]interface{})
	resp, exists, err := s.GetProperty("10", gamma)
	assert.True(t, exists)
	assert.Equal(t, resp["name"], "Hotel 1")

	resp, exists, err = s.GetProperty("20", gamma)
	assert.True(t, exists)
	assert.Equal(t, resp["name"], "Hotel 2")
	assert.Equal(t, resp["city"], "2 City")
}
