package waitforit

import (
	"fmt"
	"net"
	"sync"
	"time"
)

// WaitForIt tests and waits on the availability of a TCP host and port
// inspired by https://github.com/vishnubob/wait-for-it .It is useful for synchronizing the spin-up of
//    interdependent services, such as linked docker containers. Since it is a
//    pure bash script, it does not have any external dependencies.
func WaitForIt(services []string, timeOut time.Duration) error {
	var depChan = make(chan struct{})
	var wg sync.WaitGroup
	wg.Add(len(services))
	go func() {
		for _, s := range services {
			go func(s string) {
				defer wg.Done()
				for {
					_, err := net.Dial("tcp", s)
					if err == nil {
						return
					}
					time.Sleep(1 * time.Second)
				}
			}(s)
		}
		wg.Wait()
		close(depChan)
	}()

	select {
	case <-depChan: // services are ready
		return nil
	case <-time.After(timeOut):
		return fmt.Errorf("services aren't ready in %s", timeOut)
	}
}
