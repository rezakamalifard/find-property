package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/ilyakaznacheev/cleanenv"

	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

type store interface {
	GetProperty(id, kind string) (property map[string]interface{}, exists bool, err error)
}

const (
	alpha = "alpha"
	beta  = "beta"
	gamma = "gamma"

	// errors
	noContentFoundErr          = "no property content found"
	failedToGetPropertyDataErr = "failed to get property data"
)

// StartServer used to configure and server the API server
func StartServer(s store) error {
	var config Config
	if err := cleanenv.ReadEnv(&config); err != nil {
		return fmt.Errorf("failed to load server configs from env: %w", err)
	}

	router := mux.NewRouter()
	router.Path("/properties/{masterID}").Queries("priorities", "{priorities}").HandlerFunc(propertyHandler(s))
	router.Path("/properties/{masterID}").HandlerFunc(propertyHandler(s))

	zap.L().Info("server started", zap.String("address", config.ServerAddress))
	// get address from input
	return http.ListenAndServe(config.ServerAddress, router)
}

func propertyHandler(s store) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		var priorities = []string{alpha, beta, gamma}

		vars := mux.Vars(r)
		qp := r.FormValue("priorities")
		masterID := vars["masterID"]
		if qp != "" {
			priorities = strings.Split(qp, ",")
		}
		for _, kind := range priorities {
			property, exists, err := s.GetProperty(masterID, kind)
			if err != nil {
				zap.L().Error("failed to get property info from db", zap.Errors("errors", []error{err}))
				response := map[string]interface{}{
					"status":  http.StatusInternalServerError,
					"message": failedToGetPropertyDataErr,
				}
				w.WriteHeader(http.StatusInternalServerError)
				responseBody, _ := json.Marshal(response)
				_, _ = w.Write(responseBody)
				return
			}
			if exists {
				responseBody, _ := json.Marshal(property)
				w.WriteHeader(http.StatusOK)
				w.Write(responseBody)
				return
			}
		}

		response := map[string]interface{}{
			"status":  http.StatusNotFound,
			"message": noContentFoundErr,
		}
		w.WriteHeader(http.StatusNotFound)
		responseBody, _ := json.Marshal(response)
		_, _ = w.Write(responseBody)
	}
}
