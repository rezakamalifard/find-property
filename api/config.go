package api

// Config is used to get configs from environment variables
type Config struct {

	// server address
	ServerAddress string `env:"API_SERVER_ADDRESS"`
}
