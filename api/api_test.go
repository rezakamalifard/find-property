package api

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

type mockStore struct {
	propertyFunc func(masterID, kind string) (map[string]interface{}, bool, error)
}

func (ms mockStore) GetProperty(id, kind string) (map[string]interface{}, bool, error) {
	return ms.propertyFunc(id, kind)
}

func TestPropertyHandlerSuccess(t *testing.T) {
	masterID := "a910b387-59fc-4cf9-979f-cb10126b1f68"
	property := func(id, kind string) (map[string]interface{}, bool, error) {
		return map[string]interface{}{
			"master_id": masterID,
		}, true, nil
	}
	store := mockStore{
		propertyFunc: property,
	}
	t.Run("check url matching with master id", func(t *testing.T) {
		recorder := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", "/properties/a910b387-59fc-4cf9-979f-cb10126b1f68", nil)
		handler := http.HandlerFunc(propertyHandler(store))

		handler.ServeHTTP(recorder, req)

		response := make(map[string]interface{})
		_ = json.NewDecoder(recorder.Result().Body).Decode(&response)

		assert.Equal(t, http.StatusOK, recorder.Result().StatusCode)
		assert.Equal(t, masterID, response["master_id"])
	})

	t.Run("check url matching with query params", func(t *testing.T) {
		recorder := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", "/properties/1000009?priorities=gamma,alpha,beta", nil)
		handler := http.HandlerFunc(propertyHandler(store))

		handler.ServeHTTP(recorder, req)
		response := make(map[string]interface{})
		_ = json.NewDecoder(recorder.Result().Body).Decode(&response)

		assert.Equal(t, http.StatusOK, recorder.Result().StatusCode)
		assert.Equal(t, masterID, response["master_id"])
	})

}

func TestPropertyHandlerError(t *testing.T) {
	getPropertyErr := errors.New("get property error")
	property := func(id, kind string) (map[string]interface{}, bool, error) {
		return map[string]interface{}{}, false, getPropertyErr
	}
	store := mockStore{
		propertyFunc: property,
	}
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/properties/1000009?priorities=gamma,alpha,beta", nil)
	handler := http.HandlerFunc(propertyHandler(store))

	handler.ServeHTTP(recorder, req)
	response := make(map[string]interface{})
	_ = json.NewDecoder(recorder.Result().Body).Decode(&response)

	assert.Equal(t, http.StatusInternalServerError, recorder.Result().StatusCode)
	assert.Equal(t, failedToGetPropertyDataErr, response["message"])

}

func TestPropertyHandlerNotFound(t *testing.T) {
	property := func(id, kind string) (map[string]interface{}, bool, error) {
		return map[string]interface{}{}, false, nil
	}
	store := mockStore{
		propertyFunc: property,
	}
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/properties/1000009?priorities=gamma,alpha,beta", nil)
	handler := http.HandlerFunc(propertyHandler(store))

	handler.ServeHTTP(recorder, req)
	response := make(map[string]interface{})
	_ = json.NewDecoder(recorder.Result().Body).Decode(&response)

	assert.Equal(t, http.StatusNotFound, recorder.Result().StatusCode)
	assert.Equal(t, noContentFoundErr, response["message"])

}

func TestPropertyHandlerPriorities(t *testing.T) {
	tests := []string{alpha, beta, gamma}
	for _, tt := range tests {
		t.Run(tt, func(t *testing.T) {
			property := func(id, kind string) (map[string]interface{}, bool, error) {
				return map[string]interface{}{
					"type": tt,
				}, true, nil
			}
			store := mockStore{
				propertyFunc: property,
			}
			recorder := httptest.NewRecorder()
			req, _ := http.NewRequest("GET", "/properties/a910b387-59fc-4cf9-979f-cb10126b1f68", nil)
			handler := http.HandlerFunc(propertyHandler(store))

			handler.ServeHTTP(recorder, req)
			response := make(map[string]interface{})
			_ = json.NewDecoder(recorder.Result().Body).Decode(&response)

			assert.Equal(t, http.StatusOK, recorder.Result().StatusCode)
			assert.Equal(t, tt, response["type"])
		})
	}
}

// TODO: add table driven tests for permutations of user priorities
