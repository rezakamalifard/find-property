package storage

// Storage is used to Get and Store Mapping and Property data on data store
type Storage interface {
	StoreMapping(master, alpha, beta, gamma string) error
	GetMapping(id, kind string) (masterID string, err error)
	StoreProperty(property map[string]interface{}) error
	GetProperty(id, kind string) (property map[string]interface{}, exists bool, err error)
}

// NewStorage create new Store and returns it
func NewStorage() (Storage, error) {
	return newMongoStore()
}
