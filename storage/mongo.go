package storage

import (
	"context"
	"fmt"
	"time"

	"github.com/ilyakaznacheev/cleanenv"
	"gitlab.com/rezakamalifard/find-property/waitforit"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	databaseName           = "content"
	mappingCollectionName  = "mapping"
	propertyCollectionName = "inventory"
)

type mongoStorage struct {
	client *mongo.Client
}

func newMongoStore() (*mongoStorage, error) {
	var config Config
	if err := cleanenv.ReadEnv(&config); err != nil {
		return nil, fmt.Errorf("failed to load mongodb configs from env: %w", err)
	}

	_ = waitforit.WaitForIt([]string{
		fmt.Sprintf(config.MongoDBAddress),
	}, 15*time.Second)

	opts := options.Client().ApplyURI(fmt.Sprintf("mongodb://%s", config.MongoDBAddress))
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		return nil, fmt.Errorf("failed connecting to mongodb: %w", err)
	}

	ctx, _ = context.WithTimeout(context.Background(), 3*time.Second)
	if err = client.Ping(ctx, nil); err != nil {
		return nil, fmt.Errorf("failed pinging mongodb: %w", err)
	}
	return &mongoStorage{client: client}, nil

}

func (m *mongoStorage) StoreMapping(master, alpha, beta, gamma string) error {
	_, err := m.client.Database(databaseName).Collection(mappingCollectionName).InsertOne(context.TODO(), bson.M{
		"master": master,
		"alpha":  alpha,
		"beta":   beta,
		"gamma":  gamma,
	})
	return err
}

func (m *mongoStorage) GetMapping(id, kind string) (master string, err error) {
	res := m.client.Database(databaseName).Collection(mappingCollectionName).FindOne(context.TODO(), bson.M{
		kind: id,
	})

	if res.Err() != nil {
		if res.Err() == mongo.ErrNoDocuments {
			return "", fmt.Errorf("mapping not found from db: %w", res.Err())
		}
		return "", fmt.Errorf("failed fetching data from db: %w", res.Err())
	}

	mapping := make(map[string]interface{})
	_ = res.Decode(&mapping)
	if value, ok := mapping["master"]; ok {
		if s, ok := value.(string); ok {
			return s, nil
		}
	}
	return "", fmt.Errorf("failed decoding data from db: %w", res.Err())

}

func (m *mongoStorage) StoreProperty(property map[string]interface{}) error {
	_, err := m.client.Database(databaseName).Collection(propertyCollectionName).InsertOne(context.TODO(), property)
	return err
}

func (m *mongoStorage) GetProperty(id, kind string) (property map[string]interface{}, exists bool, err error) {
	res := m.client.Database(databaseName).Collection(propertyCollectionName).FindOne(context.TODO(), bson.M{
		"kind":      kind,
		"master_id": id,
	})
	if res.Err() != nil {
		if res.Err() == mongo.ErrNoDocuments {
			return property, false, nil
		}
		return property, false, fmt.Errorf("failed fetching data from db: %w", res.Err())
	}
	err = res.Decode(&property)
	if err != nil {
		return property, false, fmt.Errorf("failed decoding data from db: %w", res.Err())
	}
	return property, true, nil
}
