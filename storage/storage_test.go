package storage

import (
	"context"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
	"testing"
)

func storeProperty(t *testing.T) {
	t.Helper()
	c, err := newMongoStore()
	assert.NoError(t, err)
	_, err = c.client.Database(databaseName).Collection(propertyCollectionName).InsertOne(context.TODO(), bson.M{
		"master_id": "222",
		"kind":      "data",
	})
	assert.NoError(t, err)
}

func assertPropertyExists(t *testing.T, id, kind string) {
	c, err := newMongoStore()

	assert.NoError(t, err)

	res := c.client.Database(databaseName).Collection(propertyCollectionName).FindOne(context.TODO(), bson.M{
		"kind":      kind,
		"master_id": id,
	})

	assert.NoError(t, res.Err())

	property := make(map[string]interface{})
	err = res.Decode(&property)

	assert.NoError(t, err)
	assert.Equal(t, id, property["master_id"])
	assert.Equal(t, kind, property["kind"])
}

func storeMapping(t *testing.T) {
	t.Helper()
	c, err := newMongoStore()
	assert.NoError(t, err)

	_, err = c.client.Database(databaseName).Collection(mappingCollectionName).InsertOne(context.TODO(), bson.M{
		"master": "222",
		"alpha":  "data",
	})

	assert.NoError(t, err)
}

func assertMappingExists(t *testing.T, id, kind string) {
	c, err := newMongoStore()
	assert.NoError(t, err)

	res := c.client.Database(databaseName).Collection(mappingCollectionName).FindOne(context.TODO(), bson.M{
		kind: id,
	})
	assert.NoError(t, res.Err())

	mapping := make(map[string]interface{})
	err = res.Decode(&mapping)

	assert.NoError(t, err)
	assert.Equal(t, "master_key", mapping["master"])
}

func TestGetPropertySuccess(t *testing.T) {
	// running these tests need to connect to database
	if testing.Short() {
		t.Skip("skipping test in short mode")
	}
	storeProperty(t)
	storage, err := NewStorage()
	assert.NoError(t, err)

	property, exists, err := storage.GetProperty("222", "data")

	assert.NoError(t, err)
	assert.True(t, exists)
	assert.Equal(t, "222", property["master_id"])
}

func TestGetPropertyNotExists(t *testing.T) {
	// running these tests need to connect to database
	if testing.Short() {
		t.Skip("skipping test in short mode")
	}
	storage, err := NewStorage()
	assert.NoError(t, err)

	_, exists, err := storage.GetProperty("not-exists", "data")
	assert.NoError(t, err)
	assert.False(t, exists)
}

func TestStorePropertySuccess(t *testing.T) {
	// running these tests need to connect to database
	if testing.Short() {
		t.Skip("skipping test in short mode")
	}

	storage, err := NewStorage()
	assert.NoError(t, err)
	data := map[string]interface{}{
		"master_id": "222",
		"kind":      "data",
	}
	err = storage.StoreProperty(data)
	assert.NoError(t, err)
	assertPropertyExists(t, "222", "data")
}

func TestGetMappingSuccess(t *testing.T) {
	// running these tests need to connect to database
	if testing.Short() {
		t.Skip("skipping test in short mode")
	}
	storeMapping(t)
	storage, err := NewStorage()
	assert.NoError(t, err)

	masterID, err := storage.GetMapping("data", "alpha")
	assert.NoError(t, err)
	assert.Equal(t, "222", masterID)
}

func TestGetMappingFailure(t *testing.T) {
	// running these tests need to connect to database
	if testing.Short() {
		t.Skip("skipping test in short mode")
	}
	storage, err := NewStorage()
	assert.NoError(t, err)

	masterID, err := storage.GetMapping("no-found", "beta")
	assert.Error(t, err)
	assert.Equal(t, "", masterID)
}

func TestStoreMappingSuccess(t *testing.T) {
	// running these tests need to connect to database
	if testing.Short() {
		t.Skip("skipping test in short mode")
	}

	storage, err := NewStorage()
	assert.NoError(t, err)

	err = storage.StoreMapping("master_key", "alpha_key", "beta_key", "gamma_key")
	assert.NoError(t, err)
	assertMappingExists(t, "alpha_key", "alpha")
}
