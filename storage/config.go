package storage

// Config is used to get configs from environment variables
type Config struct {
	// mongodb configs
	MongoDBAddress string `env:"STORAGE_MONGO_DB_ADDRESS"`
}
