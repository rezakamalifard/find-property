# Find Property
Find Property is a tool for inserting and an API service for getting property and mapping content data from different partners (Alpha, Beta, Gamma) inventories in CSV or JSON lines format. 

## Insert tool
It is a simple tool to process mapping and inventory data from gzip files and insert them into the DB. Before storing a value into the DB insert tool gets its master_id from the mappings and store it. We don't need to check mapping before Getting property data from the DB for the API calls, and we can directly answer search queries for the provide id and desired partner type.
### Usage
## Docker compose
You can use the provided docker-compose to run a mongo database,  the insert tool on the mapping and inventory files, and finally, the API server
Set these environment variables inside the `docker-compose.yml` file before running it
```bash
INSERT_INVENTORIES_PATH="./input/inventories"
INSERT_MAPPING_FILE_PATH="./input/mapping/dump_data.csv.gz"

make up
```
You can run queries at the API endpoint.

```bash
curl "http://localhost:8080/properties/1000009?priorities=alpha,gamma,beta"
```

Set these environment variables for the insert tool to find the mapping and inventory files and get the DB address before running the tool
```bash
export INSERT_INVENTORIES_PATH="./input/inventories"
export INSERT_MAPPING_FILE_PATH="./input/mapping/dump_data.csv.gz"
export STORAGE_MONGO_DB_ADDRESS="localhost:27017"

make insert
```
```json
{"msg":"created mappings","successful":10000,"failed":0,"duration":"20.811875786s"}
{"msg":"processed alpha","successful":9997,"failed":0,"duration":"1m.691307317s"}
{"msg":"processed beta","successful":5829,"failed":0,"duration":"39.110310451s"}
{"msg":"processed gamma","successful":5221,"failed":0,"duration":"39.694110732s"}
{"msg":"successfully processed all data","duration":"3m22.337195122s"}
```
## API Service
It is a go HTTP service for getting property data from the store. 

### Usage
Set these environment variables for the API server to get the DB address and server address `host:port` before running the service.
```bash
export STORAGE_MONGO_DB_ADDRESS="localhost:27017"
export API_SERVER_ADDRESS="localhost:8080" or ":8080"

make server
```
```json
{"level":"info","ts":1608508384.4890792,"caller":"api/api.go:40","msg":"server started","address":":8080"}
```
You can run queries at the API endpoint.

```bash
curl "http://localhost:8080/properties/1000009?priorities=alpha,gamma,beta"
```
```json
{
  "_id": "5fdfe434ce984f32571aa622",
  "address": "426 Gan Gan Rd, Anna Bay, Port Stephens",
  "city": "Anna Bay",
  "country_code": "AU",
  "id": "5dbd9d51-8049-53b5-92bb-74466905bb81",
  "kind": "alpha",
  "latitude": -32.77685,
  "longitude": 152.11303,
  "master_id": "1000009",
  "name": "Ingenia Holidays One Mile Beach",
  "star_rating": 0
}
```
## API endpoint
Sever has only one endpoint `/properties`.

```bash
properties/ID?priorities=beta,alpha,gamma
```
That endpoint receives the master ID and an optional list of provider priorities.
The API response (JSON) has to build based on this priority list:

- if the content from the first provider in the list is present: the content of that provider is returned.
- if a provider doesn't have a property for a given ID: the next provider from the priority list needs to be checked
- if the provider priority list isn't specified: use default the priority: `alpha,beta,gamma`