package main

import (
	"time"

	"github.com/ilyakaznacheev/cleanenv"
	"gitlab.com/rezakamalifard/find-property/inventory"
	"gitlab.com/rezakamalifard/find-property/mapping"
	"gitlab.com/rezakamalifard/find-property/storage"
	"go.uber.org/zap"
)

func main() {
	started := time.Now()
	// create logger
	logger, _ := zap.NewProduction()
	defer logger.Sync() // flushes buffer
	zap.ReplaceGlobals(logger)

	var config Config
	if err := cleanenv.ReadEnv(&config); err != nil {
		zap.L().Panic("failed to load config from env", zap.Errors("errors", []error{err}))
	}

	store, err := storage.NewStorage()
	if err != nil {
		zap.L().Panic("failed to create store", zap.Errors("errors", []error{err}))
	}

	// create mapper
	mapper := mapping.NewMapper(store)

	// create mapping and save to storage
	if err = mapper.Create(config.MappingsPath); err != nil {
		zap.L().Panic("failed to create mapping", zap.Errors("errors", []error{err}))
	}

	// create inventory
	inv := inventory.NewInventory(store)

	// process inventory data and add parsed content to storage
	err = inv.Process(config.InventoriesPath)
	if err != nil {
		zap.L().Panic("failed to run inventory process", zap.Errors("errors", []error{err}))
	}

	// log end of process
	duration := time.Since(started)
	zap.L().Info("successfully processed all data", zap.String("duration", duration.String()))
}
