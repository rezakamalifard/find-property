package main

// Config is used to get configs from environment variables
type Config struct {
	InventoriesPath string `env:"INSERT_INVENTORIES_PATH"`
	MappingsPath    string `env:"INSERT_MAPPING_FILE_PATH"`
}
