package main

import (
	"gitlab.com/rezakamalifard/find-property/api"
	"gitlab.com/rezakamalifard/find-property/storage"
	"go.uber.org/zap"
)

func main() {
	logger, _ := zap.NewProduction()
	defer logger.Sync() // flushes logger buffer before exit if exists

	// set global logger Zap.L() to create logger
	zap.ReplaceGlobals(logger)

	// create store
	store, err := storage.NewStorage()
	if err != nil {
		zap.L().Panic("failed to create store", zap.Errors("errors", []error{err}))
	}

	// start API server
	zap.L().Fatal(api.StartServer(store).Error())
}
