APP = findproperty
TIMESTAMP?=$(shell TZ="Asia/Tehran" date +'%Y-%m-%dT%H:%M:%S%z')
GIT_HEAD_REF := $(shell cat .git/HEAD | cut -d' ' -f2)
CI_COMMIT_REF_SLUG?=$(shell cat .git/HEAD | cut -d'/' -f3)
CI_COMMIT_SHORT_SHA?=$(shell cat .git/$(GIT_HEAD_REF) | head -c 8)
DC_FILE="docker-compose.yml"
build-api:
	go build -race  -o bin/server gitlab.com/rezakamalifard/find-property/cmd/server

build-insert:
	go build -race -o bin/insert gitlab.com/rezakamalifard/find-property/cmd/insert

build: build-api build-insert

run-insert:
	go run gitlab.com/rezakamalifard/find-property/cmd/index

run-api:
	go run gitlab.com/rezakamalifard/find-property/cmd/server

insert: build-insert
	bin/insert

server: build-api
	bin/server

docker:
	docker build \
	--build-arg=GIT_BRANCH=$(CI_COMMIT_REF_SLUG) \
	--build-arg=GIT_SHA=$(CI_COMMIT_SHORT_SHA) \
    --build-arg=GIT_TAG=$(CI_COMMIT_TAG) \
	--build-arg=BUILD_TIMESTAMP=$(TIMESTAMP) \
	-t $(APP):$(CI_COMMIT_REF_SLUG) .

test:
	go test -short ./...

coverage:
	go test -race -short -v -coverprofile=.testCoverage.txt ./...
	go tool cover -func=.testCoverage.txt

coverage-report: coverage
	go tool cover -html=.testCoverage.txt -o testCoverageReport.html

up:
	docker-compose -f $(DC_FILE) up
