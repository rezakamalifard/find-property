module gitlab.com/rezakamalifard/find-property

go 1.15

require (
	github.com/fatih/structs v1.1.0
	github.com/gorilla/mux v1.8.0
	github.com/ilyakaznacheev/cleanenv v1.2.5
	github.com/stretchr/testify v1.6.1
	go.mongodb.org/mongo-driver v1.4.4
	go.uber.org/zap v1.16.0
)
