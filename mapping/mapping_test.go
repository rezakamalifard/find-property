package mapping

import (
	"compress/gzip"
	"encoding/csv"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rezakamalifard/find-property/storage"
	"os"
	"testing"
)

func createDirIfNotExist(t *testing.T, dir string) {
	t.Helper()
	_, err := os.Stat(dir)

	if os.IsNotExist(err) {
		errDir := os.MkdirAll(dir, os.ModePerm)
		assert.NoError(t, errDir)
	}
}

const mappingFileName = "dump_data.csv.gz"

func mockMappingFile(t *testing.T, records [][]string) (path string) {
	t.Helper()
	dir := os.TempDir()
	mappingDir := dir + "mapping-tests/"
	createDirIfNotExist(t, mappingDir)
	fileName := mappingDir + mappingFileName
	f, err := os.Create(fileName)
	defer f.Close()

	assert.NoError(t, err)

	gzipW := gzip.NewWriter(f)
	defer gzipW.Close()

	csvW := csv.NewWriter(gzipW)

	for _, record := range records {
		err = csvW.Write(record)
		assert.NoError(t, err)
	}

	csvW.Flush()
	assert.NoError(t, csvW.Error())

	return f.Name()
}

func TestMapping(t *testing.T) {
	// running these tests need to connect to database
	if testing.Short() {
		t.Skip("skipping test in short mode")
	}
	records := [][]string{
		{"master_id", "alpha_id", "beta_id", "gamma_id"},
		{"1", "2", "3", "4"},
		{"10", "11", "12", "13"},
	}
	p := mockMappingFile(t, records)

	// create store
	s, err := storage.NewStorage()
	assert.NoError(t, err)

	// create mapper
	m := NewMapper(s)

	err = m.Create(p)
	assert.NoError(t, err)

	masterID, err := s.GetMapping("2", "alpha")
	assert.NoError(t, err)
	assert.Equal(t, "1", masterID)

}
