package mapping

import (
	"compress/gzip"
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"time"

	"go.uber.org/zap"
)

type store interface {
	StoreMapping(master, alpha, beta, gamma string) error
	GetMapping(id, kind string) (masterID string, err error)
}

// Mapper type is used to store store data
type Mapper struct {
	Store store
}

// NewMapper creates a new mapper instance assign its variables and returns it
func NewMapper(s store) *Mapper {
	return &Mapper{Store: s}
}

// Create process mapping file and store its data on store
func (m *Mapper) Create(path string) error {
	started := time.Now()
	var successful, failed uint64
	f, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("cannot open the mapping file %w", err)
	}

	zipread, err := gzip.NewReader(f)
	if err != nil {
		return fmt.Errorf("cannot read from mapping gzip file %w", err)

	}
	reader := csv.NewReader(zipread)
	var header []string
	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			zap.L().Warn("cannot read mapping csv file record", zap.Errors("errors", []error{err}))
			failed++
			continue
		}
		// skip headers
		if header == nil {
			header = record
			continue
		}

		row := make(map[string]string)
		for i := range header {
			row[header[i]] = record[i]
		}
		master := row["master_id"]
		alpha := row["alpha_id"]
		beta := row["beta_id"]
		gamma := row["gamma_id"]

		// store values into db
		if err = m.Store.StoreMapping(master, alpha, beta, gamma); err != nil {
			zap.L().Warn("cannot store mapping to db", zap.Errors("errors", []error{err}))
			failed++
			continue
		}

		successful++
	}
	duration := time.Since(started)
	zap.L().Info(fmt.Sprintf("created mappings"),
		zap.Uint64("successful",
			successful),
		zap.Uint64("failed", failed),
		zap.String("duration", duration.String()),
	)
	return nil
}
